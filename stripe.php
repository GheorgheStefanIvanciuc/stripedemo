<html>
	<head>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
		<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
		<script type="text/javascript" src="main.js"></script>
		<script type="text/javascript">
			// Cheia publica (vine din php, dintr-un fisier de config)
			Stripe.setPublishableKey('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
		</script>
	</head>
	<body>
		<form action="backend.php" method="POST" id="payment-form">
			<span class="payment-errors"></span>

			<div class="form-row">
				<label>
					<span>Card Number(4242424242424242)</span>
					<input type="text" size="20" data-stripe="number"/>
				</label>
			</div>

			<div class="form-row">
				<label>
					<span>CVC</span>
					<input type="text" size="4" data-stripe="cvc"/>
				</label>
			</div>

			<div class="form-row">
				<label>
					<span>Expiration (MM/YYYY)</span>
					<input type="text" size="2" data-stripe="exp-month"/>
				</label>
				<span> / </span>
				<input type="text" size="4" data-stripe="exp-year"/>
			</div>

			<button type="submit">Submit Payment</button>
		</form>
	</body>
</html>